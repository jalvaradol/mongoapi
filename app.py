from flask import Flask, jsonify, request
from pymongo import MongoClient
from flask_cors import CORS
import mongoDBController as mongo
import readConfigurationsFile as conf
import json

configurations = {}
conf.loadConfiguration('settings.conf', configurations)
print(configurations)

client = MongoClient('mongodb://'+str(configurations['HOST_MONGO'])+':'+str(configurations['PORT_MONGO'])+'/')
db = client[str(configurations['DATABASE_MONGO'])]

app = Flask(__name__)
CORS(app)

@app.route('/test', methods=['GET'])
def test():
    return 'Test Correct!'

@app.route('/actions', methods=['GET'])
def get_all_actions():
    return mongo.getAllActions(db)

@app.route('/actions', methods=['POST'])
def add_action():
    action = json.loads(request.data)
    print('Adding action')
    print(action)
    return mongo.addAction(db, action)

@app.route('/log', methods=['POST'])
def add_log():
    intent_log = json.loads(request.data)
    print('Adding log')
    print(intent_log)
    return mongo.addLog(db, intent_log)

if __name__ == '__main__':
    app.run(host=str(configurations['HOST']), port=str(configurations['PORT']))