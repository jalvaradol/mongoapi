from bson.json_util import dumps
from bson.objectid import ObjectId


def getAllActions(db):
    actions = db.actions
    return dumps(list(actions.find()))


def addAction(db, action):
    actions = db.actions
    action_data = {
        'intent': action['intent'],
        'executedIntent': action['executedIntent'],
        'context': action['context']
    }
    return str(actions.insert_one(action_data).inserted_id)


def addLog(db, intent_log):
    log = db.log
    log_data = {
        'intent': intent_log['intent'],
        'context': intent_log['context'],
        'dynamic_intent': intent_log['dynamic_intent'],
        'prediction': intent_log['prediction'],
        'dynamic_executed': intent_log['dynamic_executed']
    }
    return str(log.insert_one(log_data).inserted_id)
